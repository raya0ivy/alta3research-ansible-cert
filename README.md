# mycode (Alta3 Research Ansible Cert)

Attempt to obtain a Ansible Certification from Alta3 Research. This certification should work as proof of basic proficiency with Ansible.

The following are suggested to be included to demonstate said proficiency:
> - Make an API request to http://api.open-notify.org/astros.json and assign the returned JSON to a variable. Then display some portion of that captured data.
> - Use Ansible to return some gathered facts or information gathered about a network/cloud/vendor specific infrastructure (this likely requires demonstrating proficiency with collections).
> - Use the Ansible script module to invoke a script (bash, Python, Ruby, etc.)
> - Be sure to remain idempotent (create a mechanism so your script doesn't run every time)
> - ADVANCED- Create a role or collection. Be sure to include a playbook that demonstrates functionality of your creation.
> - Use Ansible in a practical manner by automating something that you may require in your day-to-day tasks.
> - Anything else you find interesting!

The following best practices should be observed:
> - Include documentation at the top
> - Comment your code so it is clear
> - Your code should run without errors


## Getting Started

These instructions will get you a copy of the project up and running on your local machine
for development and testing purposes. See deployment for notes on how to deploy the project
on a live system.

### Prerequisites

What things are needed to install the software and how to install them. For now, maybe copy in:
"How to Install the Language: "

## Built With

* [Ansible](https://www.ansible.com) - The coding language used
* [Python](https://www.python.org/) - The coding language used

## Authors

* **Raya Ivy** - *RI* - [LinkedIn](https://linkedin.com/in/rayaivy)
